package com.example.jyothisrajan.photocap;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.label.FirebaseVisionImageLabel;
import com.google.firebase.ml.vision.label.FirebaseVisionImageLabeler;

import java.io.ByteArrayOutputStream;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class CaptionActivity extends AppCompatActivity {

    ImageView userImage;
    Button btnCaption;
    Button btnEditPhoto;
    String[] tagLines = new String[20];
    int i=0;
    Bitmap bitmap;
    private ProgressBar spinner;
    private static final String TAGLOG = "CaptionActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_caption);

        userImage = (ImageView) findViewById(R.id.ivUserImage);
        btnCaption = (Button) findViewById(R.id.btnGenerateCap);
        btnEditPhoto = (Button) findViewById(R.id.btnEditPhoto);

        spinner = (android.widget.ProgressBar)findViewById(R.id.progressBar1);

        Intent intent = getIntent();
       getIntentData();
       // bitmap = (Bitmap) intent.getParcelableExtra("BitmapImage");
        userImage.setImageBitmap(bitmap);
        btnCaption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendImageToFirebase(bitmap);

            }
        });
        btnEditPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIntentDataToEditor(tagLines);

            }
        });


    }
    void sendImageToFirebase(Bitmap bitmap){
        spinner.setVisibility(View.VISIBLE);
        i=0;
        //editTextCap.setText(" ");

        FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(bitmap);
        FirebaseVisionImageLabeler labeler = FirebaseVision.getInstance()
                .getOnDeviceImageLabeler();
        labeler.processImage(image)
                .addOnSuccessListener(new OnSuccessListener<List<FirebaseVisionImageLabel>>() {
                    @Override
                    public void onSuccess(List<FirebaseVisionImageLabel> labels) {
                        // Task completed successfully
                        // ...

                        for (FirebaseVisionImageLabel label: labels) {
                            String text = label.getText();
                            String entityId = label.getEntityId();

                            float confidence = label.getConfidence();

                                tagLines[i] = label.getText();
                                i++;

                            //editTextCap.setText(editTextCap.getText()+"#"+label.getText());
                            Log.d(TAGLOG +i,""+label);

                            //Toast.makeText(MainActivity.this, ""+text, Toast.LENGTH_SHORT).show();
                        }
                        Log.d(TAGLOG,""+tagLines[1]);
                        sendIntentData(tagLines);







                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Task failed with an exception
                        // ...
                        Toast.makeText(CaptionActivity.this, "Try again later", Toast.LENGTH_SHORT).show();
                    }
                });



    }
    void sendIntentData(String[] tagLinesStrings){
        spinner.setVisibility(View.GONE);
        Intent intent = new Intent(this, ListCaptionActivity.class);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        intent.putExtra("BitmapImage", b);
        intent.putExtra("tagLinesStrings", tagLinesStrings);
        startActivity(intent);


    }
    void sendIntentDataToEditor(String[] tagLinesStrings){
        spinner.setVisibility(View.GONE);
        Intent intent = new Intent(this, ImageEditorDisplayActivity.class);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        intent.putExtra("BitmapImage", b);
        intent.putExtra("tagLinesStrings", tagLinesStrings);
        startActivity(intent);


    }
    void getIntentData(){
        Bundle extras = getIntent().getExtras();
        byte[] b = extras.getByteArray("BitmapImage");
        bitmap = BitmapFactory.decodeByteArray(b, 0, b.length);
    }
}

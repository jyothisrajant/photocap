package com.example.jyothisrajan.photocap;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class ListCaptionActivity extends AppCompatActivity{

    RecyclerView recyclerViewFirst;
    List<CaptionListResponse> captionListResponseData;
    CaptionAdapter captionAdaptar;
    Button btnContinue;
    Bitmap bitmap;
    String[] tagLinesStrings;
    SharedCaption sharedCaption;
    String selectedCaption;
    String BASE_URL="https://good-quotes.p.rapidapi.com/tag/";
    String totalResponse;
    Response<String> totalResponseArray;
    ArrayList<CaptionListResponse> captionArrayList = new ArrayList<>();
    private ProgressBar spinner;
    private static final String TAGLOG = "ListCaptionActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listcaption);
        getIntentData();
        Log.d("ListCaptionActivity",""+tagLinesStrings[1]);
        recyclerViewFirst = (RecyclerView) findViewById(R.id.recyclerViewFirst);
        btnContinue = (Button) findViewById(R.id.btnContinue);
        spinner = (android.widget.ProgressBar)findViewById(R.id.progressBar1);
        sharedCaption=new SharedCaption();
        getCaptionListFirstData();
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("custom-message"));
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
               // Toast.makeText(ListCaptionActivity.this, ""+sharedCaption.getQuote(), Toast.LENGTH_SHORT).show();
                sendIntentData(bitmap);
            }
        });

    }
    private void getCaptionListFirstData() {
        spinner.setVisibility(View.VISIBLE);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiInterface.JSONURL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);
        //api.getDataApi("sky");

        Call<String> call = api.getString(""+BASE_URL+tagLinesStrings[0]);




        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.i(TAGLOG, response.body().toString());
                //Toast.makeText()
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Log.i(TAGLOG, response.body().toString());

                        String jsonresponse = response.body().toString();
                        setData(jsonresponse);
                        getCaptionListSecondData();

                    } else {
                        Log.i(TAGLOG, "Returned empty response");//Toast.makeText(getContext(),"Nothing returned",Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }
    private void getCaptionListSecondData() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiInterface.JSONURL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);
        //api.getDataApi("sky");

        Call<String> call = api.getString(""+BASE_URL+tagLinesStrings[1]);




        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.i(TAGLOG, response.body().toString());
                //Toast.makeText()
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Log.i(TAGLOG, response.body().toString());

                        String jsonresponse = response.body().toString();
                        //setDataInRecyclerView(jsonresponse,recyclerViewSecond);
                        setData(jsonresponse);
                        getCaptionListThirdData();

                    } else {
                        Log.i("onEmptyResponse", "Returned empty response");//Toast.makeText(getContext(),"Nothing returned",Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }
    private void getCaptionListThirdData() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiInterface.JSONURL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);
        //api.getDataApi("sky");

        Call<String> call = api.getString(""+BASE_URL+tagLinesStrings[2]);




        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.i("Responsestring", response.body().toString());
                //Toast.makeText()
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Log.i("onSuccessThird", response.body().toString());

                        String jsonresponse = response.body().toString();
                        Log.i("onSuccessFinal", response.body().toString());
                        setData(jsonresponse);
                        setRecyclerView();

                    } else {
                        Log.i("onEmptyResponse", "Returned empty response");//Toast.makeText(getContext(),"Nothing returned",Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    private void setData(String response) {

        try {
            //getting the whole json object from the response
            JSONObject obj = new JSONObject(response);

            Log.i("onSuccess", "Enter inside");

            JSONArray dataArray  = obj.getJSONArray("quotes");

            for (int i = 0; i < dataArray.length(); i++) {

                CaptionListResponse captionListResponse = new CaptionListResponse();
                JSONObject dataobj = dataArray.getJSONObject(i);

                // modelRecycler.setImgURL(dataobj.getString("imgURL"));
                captionListResponse.setQuote(dataobj.getString("quote"));
                Log.i("onSuccess", dataobj.getString("quote"));



                captionArrayList.add(captionListResponse);





            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    void getIntentData(){
        Intent intent = getIntent();
        tagLinesStrings = intent.getStringArrayExtra("tagLinesStrings");
        Log.d("StringList",tagLinesStrings[2]);
        Bundle extras = getIntent().getExtras();
        byte[] b = extras.getByteArray("BitmapImage");
        bitmap = BitmapFactory.decodeByteArray(b, 0, b.length);
    }
    void sendIntentData(Bitmap bitmap){
        //spinner.setVisibility(View.GONE);
        Intent intent = new Intent(this, ShareActivity.class);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        intent.putExtra("BitmapImage", b);
        intent.putExtra("tagLinesStrings", tagLinesStrings);
        intent.putExtra("sharedCaption", selectedCaption);
        startActivity(intent);


    }
    public BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            selectedCaption = intent.getStringExtra("selectedcaption");
           // Toast.makeText(ListCaptionActivity.this,""+selectedCaption ,Toast.LENGTH_SHORT).show();
        }
    };

    void setRecyclerView(){
        captionAdaptar = new CaptionAdapter(this,captionArrayList);
        recyclerViewFirst.setAdapter(captionAdaptar);
        recyclerViewFirst.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
        spinner.setVisibility(View.GONE);

    }

}

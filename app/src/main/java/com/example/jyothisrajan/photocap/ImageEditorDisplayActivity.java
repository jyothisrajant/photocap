package com.example.jyothisrajan.photocap;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.util.Pair;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;

public class ImageEditorDisplayActivity extends AppCompatActivity {

    static Bitmap bitmap;
    static float vH=0,vW=0;
    static BitmapFactory.Options bmOptions;
    private final static String TAG = "DEBUG_BOTTOM_NAV_UTIL";
    static PhotoView imageDisplay;
    static float iHeight = 0;
    String[] tagLinesStrings;
    float targetW;
    float targetH;
    private static final String TAGLOG = "ImageEditorDisplayActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        setContentView(R.layout.activity_editor);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        imageDisplay = (PhotoView) findViewById(R.id.imageDisplay);

        targetW = getIntent().getExtras().getInt("ImageHeight");
        targetH = getIntent().getExtras().getInt("ImageWidth");

        getIntentData();

        imageDisplay.setImageBitmap(bitmap);


        final BottomNavigationView optionNavigationView = (BottomNavigationView)findViewById(R.id.optionNavigation);

        optionNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.action_addText:

                        sendIntentDataToAddText(tagLinesStrings,bitmap);

                        break;
                    case R.id.action_addEmoji:
                        Intent emojiIntent = new Intent(ImageEditorDisplayActivity.this, AddStickerActivity.class);
                        sendIntentDataToAddSticker(tagLinesStrings,bitmap);
//                        emojiIntent.putExtra("height", targetH);
//                        emojiIntent.putExtra("width", targetW);
//                        ActivityOptionsCompat optionsEmoji = ActivityOptionsCompat.makeSceneTransitionAnimation(ImageEditorDisplayActivity.this, new Pair<View, String>(findViewById(R.id.imageDisplay), (getString(R.string.transition_image))));
//                        ActivityCompat.startActivity(ImageEditorDisplayActivity.this, emojiIntent, optionsEmoji.toBundle());
                        break;
                    case R.id.action_rotateCrop:
                        Intent rotCropIntent = new Intent(ImageEditorDisplayActivity.this, AddCropActivity.class);
                        rotCropIntent.putExtra("height", targetH);
                        rotCropIntent.putExtra("width", targetW);
                        startActivity(rotCropIntent);

                        break;
                }

                return true;
            }
        });

        ImageView saveDisplayImage = (ImageView)findViewById(R.id.saveImageDisplay);
        saveDisplayImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    saveImage();
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        });

        ImageView cancelDisplayImage = (ImageView)findViewById(R.id.cancelImageDisplay);
        cancelDisplayImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = getBaseContext().getPackageManager().getLaunchIntentForPackage( getBaseContext().getPackageName() );
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });



    }


    private void saveImage(){
        sendIntentDataToCaptionActivity(tagLinesStrings,bitmap);
        finish();

    }



    void sendIntentDataToAddText(String[] tagLinesStrings,Bitmap bitmap){
        //spinner.setVisibility(View.GONE);
        Intent intent = new Intent(this, AddTextActivity.class);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        intent.putExtra("BitmapImage", b);
        intent.putExtra("tagLinesStrings", tagLinesStrings);
        intent.putExtra("ImageHeight", targetH);
        intent.putExtra("ImageWidth", targetW);
        startActivity(intent);


    }
    void sendIntentDataToAddSticker(String[] tagLinesStrings,Bitmap bitmap){
        //spinner.setVisibility(View.GONE);
        Intent intent = new Intent(this, AddStickerActivity.class);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        intent.putExtra("BitmapImage", b);
        intent.putExtra("tagLinesStrings", tagLinesStrings);
        intent.putExtra("height", targetH);
        intent.putExtra("width", targetW);
        startActivity(intent);


    }
    void sendIntentDataToCaptionActivity(String[] tagLinesStrings,Bitmap bitmap){
        //spinner.setVisibility(View.GONE);
        Intent intent = new Intent(this, CaptionActivity.class);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        intent.putExtra("BitmapImage", b);
        intent.putExtra("tagLinesStrings", tagLinesStrings);
        startActivity(intent);


    }
    void getIntentData(){
        Intent intent = getIntent();
        tagLinesStrings = intent.getStringArrayExtra("tagLinesStrings");
        Bundle extras = getIntent().getExtras();
        byte[] b = extras.getByteArray("BitmapImage");
        bitmap = BitmapFactory.decodeByteArray(b, 0, b.length);
    }

}

package com.example.jyothisrajan.photocap;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Url;

public interface ApiInterface {

    

    String JSONURL = "https://good-quotes.p.rapidapi.com/";
    @Headers({
            "x-rapidapi-host: good-quotes.p.rapidapi.com",
            "x-rapidapi-key: 5c1eb961abmsh86ac4428d1a8efbp1b39b5jsn53fc90647f43"
    })

    @GET
    Call<String> getString(@Url String url);


}

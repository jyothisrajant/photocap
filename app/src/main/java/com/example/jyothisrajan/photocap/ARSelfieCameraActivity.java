package com.example.jyothisrajan.photocap;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Camera;
import android.graphics.Color;
import android.graphics.Matrix;
import android.os.Bundle;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;
import com.google.android.material.snackbar.Snackbar;
import com.example.jyothisrajan.photocap.CameraSourcePreview;
import com.example.jyothisrajan.photocap.GraphicOverlay;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import static android.view.View.GONE;

public class ARSelfieCameraActivity extends AppCompatActivity {
    TextGraphic mTextGraphic;
    private int cameraId;
    private static final String TAGLOG = "ARSelfieCameraActivity";

    private final Thread observer = new Thread("observer") {

        {
            setDaemon(true);
        }

        public void run() {

            while( !isInterrupted() ) {
                /*
                TextGraphic mTextGraphic = new TextGraphic(mGraphicOverlay);
                mGraphicOverlay.add(mTextGraphic);*/
                //mTextGraphic.updateText(2);
            }

        };
    };

    private static final String TAG = "FaceTracker";

    private CameraSource mCameraSource = null;
    private int typeFace = 0;
    private int typeFlash = 0;
    private boolean flashmode = false;
    private Camera camera;

    private static final int MASK[] = {
            R.id.no_filter,
            R.id.sticker_glass,
            R.id.sticker_smiley1,
            R.id.sticker_glass2
    };

    private CameraSourcePreview mPreview;
    private GraphicOverlay mGraphicOverlay;

    private static final int RC_HANDLE_GMS = 9001;
    // permission request codes need to be < 256
    private static final int RC_HANDLE_CAMERA_PERM = 2;
    Bitmap bitmap;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_ar_selfie);

        mPreview = (CameraSourcePreview) findViewById(R.id.preview);
        mGraphicOverlay = (GraphicOverlay) findViewById(R.id.faceOverlay);
        //mTextGraphic = new TextGraphic(mGraphicOverlay);
        //mGraphicOverlay.add(mTextGraphic);
        cameraId = android.hardware.Camera.CameraInfo.CAMERA_FACING_BACK;

        ImageButton face = (ImageButton) findViewById(R.id.face);
        face.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(findViewById(R.id.scrollView).getVisibility() == GONE){
                    findViewById(R.id.scrollView).setVisibility(View.VISIBLE);
                    ((ImageButton) findViewById(R.id.face)).setImageResource(R.drawable.camera_smile_select);
                }else{
                    findViewById(R.id.scrollView).setVisibility(GONE);
                    ((ImageButton) findViewById(R.id.face)).setImageResource(R.drawable.camera_smile);
                }
            }
        });

        ImageButton no_filter = (ImageButton) findViewById(R.id.no_filter);
        no_filter.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                findViewById(MASK[typeFace]).setBackgroundResource(R.drawable.filter_background);
                typeFace = 0;
                findViewById(MASK[typeFace]).setBackgroundResource(R.drawable.filter_background_select);
            }
        });


        ImageButton sticker_glass2 = (ImageButton) findViewById(R.id.sticker_glass);
        sticker_glass2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                findViewById(MASK[typeFace]).setBackgroundResource(R.drawable.filter_background);
                typeFace = 1;
                findViewById(MASK[typeFace]).setBackgroundResource(R.drawable.filter_background_select);
            }
        });
        ImageButton sticker_smiley1 = (ImageButton) findViewById(R.id.sticker_smiley1);
        sticker_smiley1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                findViewById(MASK[typeFace]).setBackgroundResource(R.drawable.filter_background);
                typeFace = 2;
                findViewById(MASK[typeFace]).setBackgroundResource(R.drawable.filter_background_select);
            }
        });
        ImageButton sticker_glass3 = (ImageButton) findViewById(R.id.sticker_glass2);
        sticker_smiley1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                findViewById(MASK[typeFace]).setBackgroundResource(R.drawable.filter_background);
                typeFace = 3;
                findViewById(MASK[typeFace]).setBackgroundResource(R.drawable.filter_background_select);
            }
        });



        ImageButton button = (ImageButton) findViewById(R.id.change);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                sendBitmapToCaptionActivity(bitmap);
            }
        });

        ImageButton camera = (ImageButton) findViewById(R.id.camera);
        camera.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                takeImage();
                //onPause();
            }
        });




        int rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (rc == PackageManager.PERMISSION_GRANTED) {
            createCameraSource();
        } else {
            requestCameraPermission();
        }
    }

    private void takeImage() {

        mCameraSource.takePicture(null, new CameraSource.PictureCallback() {
                    @Override
                    public void onPictureTaken(byte[] bytes) {
                        Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                        Log.d("TAGLOG", bmp.getWidth() + "x" + bmp.getHeight());
                        onPause();
                        //sendBitmapToCaptionActivity(bmp);
                        //bitmap=bmp;
                    }
                });
//        try{

//            mCameraSource.takePicture(null, new CameraSource.PictureCallback() {
//
//                private File imageFile;
//                @Override
//                public void onPictureTaken(byte[] bytes) {
//                    try {
//                        // convert byte array into bitmap
//                        Bitmap loadedImage = null;
//                        Bitmap rotatedBitmap = null;
//                        loadedImage = BitmapFactory.decodeByteArray(bytes, 0,
//                                bytes.length);
//                        bitmap=loadedImage;
//                        //sendBitmapToCaptionActivity(loadedImage);
//
////                        // rotate Image
////                        Matrix rotateMatrix = new Matrix();
////                        //rotateMatrix.postRotate(rotation);
////                        rotatedBitmap = Bitmap.createBitmap(loadedImage, 0, 0,
////                                loadedImage.getWidth(), loadedImage.getHeight(),
////                                rotateMatrix, false);
////                        String state = Environment.getExternalStorageState();
////                        File folder = null;
////                        if (state.contains(Environment.MEDIA_MOUNTED)) {
////                            folder = new File(Environment
////                                    .getExternalStorageDirectory() + "/Demo");
////                        } else {
////                            folder = new File(Environment
////                                    .getExternalStorageDirectory() + "/Demo");
////                        }
////
////                        boolean success = true;
////                        if (!folder.exists()) {
////                            success = folder.mkdirs();
////                        }
////                        if (success) {
////                            java.util.Date date = new java.util.Date();
////                            imageFile = new File(folder.getAbsolutePath()
////                                    + File.separator
////                                    //+ new Timestamp(date.getTime()).toString()
////                                    + "Image.jpg");
////
////                            imageFile.createNewFile();
////                        } else {
////                            Toast.makeText(getBaseContext(), "Image Not saved",
////                                    Toast.LENGTH_SHORT).show();
////                            return;
////                        }
////
////                        ByteArrayOutputStream ostream = new ByteArrayOutputStream();
////
////                        // save image into gallery
////                        rotatedBitmap = resize(rotatedBitmap, 800, 600);
////                        rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, ostream);
////
////                        FileOutputStream fout = new FileOutputStream(imageFile);
////                        fout.write(ostream.toByteArray());
////                        fout.close();
////                        ContentValues values = new ContentValues();
////
////                        values.put(MediaStore.Images.Media.DATE_TAKEN,
////                                System.currentTimeMillis());
////                        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
////                        values.put(MediaStore.MediaColumns.DATA,
////                                imageFile.getAbsolutePath());
////
////                        ARSelfieCameraActivity.this.getContentResolver().insert(
////                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
//
//                        setResult(Activity.RESULT_OK); //add this
//                       // finish();
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            });
//
//        }catch (Exception ex){
//        }

    }




    private void requestCameraPermission() {
        Log.w(TAGLOG, "Camera permission is not granted. Requesting permission");

        final String[] permissions = new String[]{Manifest.permission.CAMERA};

        if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(this, permissions, RC_HANDLE_CAMERA_PERM);
            return;
        }

        final Activity thisActivity = this;

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityCompat.requestPermissions(thisActivity, permissions,
                        RC_HANDLE_CAMERA_PERM);
            }
        };

        Snackbar.make(mGraphicOverlay, "Camera permission needed",
                Snackbar.LENGTH_INDEFINITE)
                .setAction("Ok", listener)
                .show();
    }


    private void createCameraSource() {

        Context context = getApplicationContext();
        FaceDetector detector = new FaceDetector.Builder(context)
                .setClassificationType(FaceDetector.ALL_CLASSIFICATIONS)
                .setLandmarkType(FaceDetector.ALL_LANDMARKS)
                .setMode(FaceDetector.ACCURATE_MODE)
                .build();

        detector.setProcessor(
                new MultiProcessor.Builder<>(new GraphicFaceTrackerFactory())
                        .build());


        if (!detector.isOperational()) {
            Log.w(TAGLOG, "Face detector dependencies are not yet available.");
        }

        mCameraSource = new CameraSource.Builder(context, detector)
                .setRequestedPreviewSize(640, 480)
                .setAutoFocusEnabled(true)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setRequestedFps(30.0f)
                .build();
    }

    @Override
    protected void onResume() {
        super.onResume();

        startCameraSource();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPreview.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCameraSource != null) {
            mCameraSource.release();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode != RC_HANDLE_CAMERA_PERM) {
            Log.d(TAGLOG, "Got unexpected permission result: " + requestCode);
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAGLOG, "Camera permission granted - initialize the camera source");
            createCameraSource();
            return;
        }

        Log.e(TAGLOG, "Permission not granted: results len = " + grantResults.length +
                " Result code = " + (grantResults.length > 0 ? grantResults[0] : "(empty)"));

        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Face Tracker sample")
                .setMessage("No permission")
                .setPositiveButton("ok", listener)
                .show();
    }

    private void startCameraSource() {


        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
                getApplicationContext());
        if (code != ConnectionResult.SUCCESS) {
            Dialog dlg =
                    GoogleApiAvailability.getInstance().getErrorDialog(this, code, RC_HANDLE_GMS);
            dlg.show();
        }

        if (mCameraSource != null) {
            try {
                mPreview.start(mCameraSource, mGraphicOverlay);
            } catch (IOException e) {
                Log.e(TAG, "Unable to start camera source.", e);
                mCameraSource.release();
                mCameraSource = null;
            }
        }
    }

    private class GraphicTextTrackerFactory implements MultiProcessor.Factory<String> {
        @Override
        public Tracker<String> create(String face) {
            return new GraphicTextTracker(mGraphicOverlay);
        }
    }

    private class GraphicTextTracker extends Tracker<String> {
        private GraphicOverlay mOverlay;
        private TextGraphic mTextGraphic ;

        GraphicTextTracker(GraphicOverlay overlay) {
            mOverlay = overlay;
            mTextGraphic = new TextGraphic(overlay);
        }

        public void onUpdate() {
            mOverlay.add(mTextGraphic);
            mTextGraphic.updateText(3);
        }

        @Override
        public void onDone() {
            mOverlay.remove(mTextGraphic);
        }
    }

    private class GraphicFaceTrackerFactory implements MultiProcessor.Factory<Face> {
        @Override
        public Tracker<Face> create(Face face) {
            return new GraphicFaceTracker(mGraphicOverlay);
        }
    }

    private class GraphicFaceTracker extends Tracker<Face> {
        private GraphicOverlay mOverlay;
        private FaceGraphic mFaceGraphic;

        GraphicFaceTracker(GraphicOverlay overlay) {
            mOverlay = overlay;
            mFaceGraphic = new FaceGraphic(overlay,typeFace);
        }

        @Override
        public void onNewItem(int faceId, Face item) {
            mFaceGraphic.setId(faceId);
        }


        @Override
        public void onUpdate(FaceDetector.Detections<Face> detectionResults, Face face) {
            mOverlay.add(mFaceGraphic);
            mFaceGraphic.updateFace(face,typeFace);
        }

        @Override
        public void onMissing(FaceDetector.Detections<Face> detectionResults) {
            mOverlay.remove(mFaceGraphic);
        }


        @Override
        public void onDone() {
            mOverlay.remove(mFaceGraphic);
        }
    }

    void sendBitmapToCaptionActivity(Bitmap bitmap){
        Intent intent = new Intent(this, CaptionActivity.class);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        Log.d(TAGLOG,"inside");
        byte[] b = baos.toByteArray();
        intent.putExtra("BitmapImage", b);
        intent.putExtra("ImageHeight", bitmap.getHeight());
        intent.putExtra("ImageWidth", bitmap.getWidth());
        startActivity(intent);


    }
//    private void flipCamera() {
//        int id = (cameraId == android.hardware.Camera.CameraInfo.CAMERA_FACING_BACK ? android.hardware.Camera.CameraInfo.CAMERA_FACING_FRONT
//                : android.hardware.Camera.CameraInfo.CAMERA_FACING_BACK);
//        if (!openCamera(id)) {
//            alertCameraDialog();
//        }
//
//    }
}

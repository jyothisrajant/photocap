package com.example.jyothisrajan.photocap;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;

import java.io.ByteArrayOutputStream;

import androidx.appcompat.app.AppCompatActivity;

public class ShareActivity extends AppCompatActivity {
    Button btnFacebook,btnEmail,btnTwitter,btnWhatsapp;
    CallbackManager callbackManager;
    ShareDialog shareDialog;
    Bitmap bitmap;
    String[] tagLinesStrings;
    String stringToShare;
    EditText edCaption;
    ImageView ivImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        setContentView(R.layout.activity_share);
        btnFacebook = (Button) findViewById(R.id.btnWhatsapp);
        btnEmail=(Button) findViewById(R.id.btnEmail);
        edCaption=(EditText) findViewById(R.id.edCaption);
        ivImage=(ImageView) findViewById(R.id.ivImage);
        btnWhatsapp=(Button) findViewById(R.id.btnInstagram);
        btnTwitter=(Button) findViewById(R.id.btnTwitter);


        //stringToShare="hellooo";

        callbackManager=CallbackManager.Factory.create();
        shareDialog=new ShareDialog(this);
        getIntentData();
        addTagLines();
        edCaption.setText(""+stringToShare);
        ivImage.setImageBitmap(bitmap);

        btnWhatsapp.setOnClickListener(new View.OnClickListener() {

            @Override
    public void onClick(View v) {
        stringToShare=edCaption.getText().toString();
        shareImage(getImageUri(bitmap),stringToShare);
    }
});
        btnFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                shareToWhatsapp(getImageUri(bitmap),stringToShare);

            }
        });


        btnEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stringToShare=edCaption.getText().toString();
                shareToGmail(getImageUri(bitmap),stringToShare);
            }
        });

        btnTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stringToShare=edCaption.getText().toString();
                shareToTwitter(getImageUri(bitmap),stringToShare);
            }
        });
    }


    // Share image
    private void shareImage(Uri imagePath,String text) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        sharingIntent.setType("image/*");
        sharingIntent.setType("text/plain");// Plain format text
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, text);
        sharingIntent.putExtra(Intent.EXTRA_STREAM, imagePath);
        //sharingIntent.setPackage("com.whatsapp.android");
        startActivity(Intent.createChooser(sharingIntent, "Share Image & Caption Using"));
    }
    private void shareToGmail(Uri imagePath,String text){
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        sharingIntent.setType("image/*");
        sharingIntent.setType("text/plain");// Plain format text
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, text);
        sharingIntent.putExtra(Intent.EXTRA_STREAM, imagePath);
        sharingIntent.setPackage("com.google.android.gm");
        startActivity(Intent.createChooser(sharingIntent, "Share Image & Caption Using"));
    }
    private void shareToTwitter(Uri imagePath,String text){
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        sharingIntent.setType("image/*");
        sharingIntent.setType("text/plain");// Plain format text
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, text);
        sharingIntent.putExtra(Intent.EXTRA_STREAM, imagePath);
        sharingIntent.setPackage("com.twitter.android");
        startActivity(Intent.createChooser(sharingIntent, "Share Image & Caption Using"));
    }
    private void shareToWhatsapp(Uri imagePath,String text){
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        //sharingIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, text);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_STREAM, imagePath);
        sharingIntent.setType("image/jpeg");
        sharingIntent.setPackage("com.whatsapp");
        startActivity(Intent.createChooser(sharingIntent, "Share Image & Caption Using"));
    }

//    // Share text
//    private void shareText(String text) {
//
//        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
//        sharingIntent.setType("text/plain");// Plain format text
//
//        // You can add subject also
//        /*
//         * sharingIntent.putExtra( android.content.Intent.EXTRA_SUBJECT,
//         * "Subject Here");
//         */
//        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, text);
//        startActivity(Intent.createChooser(sharingIntent, "Share Text Using"));
//    }

    private void sharetoFacebook(){


        SharePhoto sharePhoto=new SharePhoto.Builder()
                .setBitmap(bitmap)
                .setCaption("adkjadj")
                .build();
        if(ShareDialog.canShow(SharePhotoContent.class)){
            SharePhotoContent content=new SharePhotoContent.Builder()
                    .addPhoto(sharePhoto)
                    .build();
            shareDialog.show(content);
        }
    }
    void getIntentData(){
        Intent intent = getIntent();
        tagLinesStrings = intent.getStringArrayExtra("tagLinesStrings");
        stringToShare = intent.getStringExtra("sharedCaption");
        //Log.d("StringList",tagLinesStrings[2]);
        Bundle extras = getIntent().getExtras();
        byte[] b = extras.getByteArray("BitmapImage");
        bitmap = BitmapFactory.decodeByteArray(b, 0, b.length);
    }

    private Uri getImageUri(Bitmap bitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        if (bitmap != null) {
            String path = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "image", null);
            return Uri.parse(path);
        } else {
            return Uri.parse("");
        }
    }
    void addTagLines(){
                for(int i=0;i<tagLinesStrings.length;i++){
                    if(tagLinesStrings[i]!=null) {
                        stringToShare = stringToShare + " #" + tagLinesStrings[i];
                    }
            Log.d("StringListAdd",stringToShare);
        }
    }

}

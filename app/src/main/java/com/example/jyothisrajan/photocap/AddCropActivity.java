package com.example.jyothisrajan.photocap;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImageView;



import androidx.appcompat.app.AppCompatActivity;

public class AddCropActivity extends AppCompatActivity {

    Bitmap textBit = ImageEditorDisplayActivity.bitmap;
    CropImageView cropImageView;
    int rot = 0;
    private static final String TAGLOG = "AddCropActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_crop);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        cropImageView = (CropImageView) findViewById(R.id.cropImageView);
        cropImageView.setImageBitmap(textBit);
        cropImageView.setFixedAspectRatio(false);
        cropImageView.setGuidelines(CropImageView.Guidelines.ON);

        final ImageView cropBitmapIcon = (ImageView) findViewById(R.id.cropBitmapIcon);
        cropBitmapIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textBit = ImageEditorDisplayActivity.bitmap = cropImageView.getCroppedImage();
                (ImageEditorDisplayActivity.imageDisplay).setImageBitmap(ImageEditorDisplayActivity.bitmap);
                Rect wh = cropImageView.getCropRect();
                ImageEditorDisplayActivity.iHeight = wh.height();
                Toast.makeText(getApplicationContext(), "Crop Applied", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        SeekBar rotateBar = (SeekBar) findViewById(R.id.rotateBar);
        rotateBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                cropImageView.setRotatedDegrees(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        ImageView cancelCropIcon = (ImageView) findViewById(R.id.cancelCropIcon);
        cancelCropIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage(getBaseContext().getPackageName());
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });

        ImageView saveCroppedIcon = (ImageView) findViewById(R.id.saveCropIcon);
        saveCroppedIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textBit = ImageEditorDisplayActivity.bitmap = cropImageView.getCroppedImage();
                try {
                    //saveImage();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        CheckBox fixedAspectRatioCheck = (CheckBox) findViewById(R.id.fixedAspectRatioCheck);
        fixedAspectRatioCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    cropImageView.setFixedAspectRatio(true);
                } else {
                    cropImageView.setFixedAspectRatio(false);
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.rot_right_action, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.rotate_right:
                rot += 90;
                if (rot > 360) {
                    rot -= 360;
                }
                cropImageView.setRotatedDegrees(rot);
                return true;
            case R.id.rotate_left:
                rot += 270;
                if (rot > 360) {
                    rot -= 360;
                }
                cropImageView.setRotatedDegrees(rot);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
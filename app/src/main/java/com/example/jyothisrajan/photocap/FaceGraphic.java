package com.example.jyothisrajan.photocap;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.google.android.gms.vision.face.Face;

class FaceGraphic extends GraphicOverlay.Graphic {


    private static final int MASK[] = {
            R.drawable.transparent,
            R.drawable.sticker_glasses2,
            R.drawable.sticker_smiley1,
            R.drawable.sticker_glass3
    };


    private volatile Face mFace;
    private int mFaceId;
    private float mFaceHappiness;
    private Bitmap bitmap;
    private Bitmap op;

    FaceGraphic(GraphicOverlay overlay,int c) {
        super(overlay);

        bitmap = BitmapFactory.decodeResource(getOverlay().getContext().getResources(),MASK[c]);
        op = bitmap;
    }

    void setId(int id) {
        mFaceId = id;
    }

    void updateFace(Face face,int c) {
        mFace = face;
        bitmap = BitmapFactory.decodeResource(getOverlay().getContext().getResources(), MASK[c]);
        op = bitmap;
        op = Bitmap.createScaledBitmap(op, (int) scaleX(face.getWidth()),
                (int) scaleY(((bitmap.getHeight() * face.getWidth()) / bitmap.getWidth())), false);
        postInvalidate();
    }


    @Override
    public void draw(Canvas canvas) {
        Face face = mFace;
        if(face == null) return;
        float x = translateX(face.getPosition().x + face.getWidth() / 2);
        float y = translateY(face.getPosition().y + face.getHeight() / 2);
        float xOffset = scaleX(face.getWidth() / 2.0f);
        float yOffset = scaleY(face.getHeight() / 2.0f);
        float left = x - xOffset;
        float top = y - yOffset;
        canvas.drawBitmap(op, left, top, new Paint());
    }

}


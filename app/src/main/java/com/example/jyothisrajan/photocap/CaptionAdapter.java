package com.example.jyothisrajan.photocap;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;


public class CaptionAdapter extends RecyclerView.Adapter<CaptionAdapter.CaptionViewHolder> {

    Context context;
    List<CaptionListResponse> captionListResponseData;
    String selectedString;
    int selected_position;
    SharedCaption sharedCaption;

    public CaptionAdapter(Context context, List<CaptionListResponse> userListResponseData) {
        this.captionListResponseData = userListResponseData;
        this.context = context;
    }

    @Override
    public CaptionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.caption_list, null);
        CaptionViewHolder captionViewHolder = new CaptionViewHolder(view);
        sharedCaption=new SharedCaption();
        return captionViewHolder;
    }

    @Override
    public void onBindViewHolder(CaptionViewHolder holder, final int position) {
        // set the data


        // Here I am just highlighting the background
        holder.itemView.setBackgroundColor(selected_position == position ? Color.GREEN : Color.TRANSPARENT);
        //holder.captionTxt.setText("" + captionListResponseData.get(position).getQuotes().get(position).getQuote());
        holder.captionTxt.setText(captionListResponseData.get(position).getQuote());
        //Log.d("FirebaseLabelsRec",""+captionListResponseData.get(position).getQuotes().get(position).getQuote());
        // implement setONCLickListtener on itemView
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // display a toast with user name
                //Toast.makeText(context, captionListResponseData.get(position).getQuotes(), Toast.LENGTH_SHORT).show();
                if (position == RecyclerView.NO_POSITION) return;

                // Updating old as well as new positions
                notifyItemChanged(selected_position);
                selected_position = position;
                notifyItemChanged(selected_position);
                selectedString=holder.captionTxt.getText().toString();
                sharedCaption.setQuote(""+selectedString);
                Log.d("Recyclerclick",""+sharedCaption.getQuote());
                Intent intent = new Intent("custom-message");
                //            intent.putExtra("quantity",Integer.parseInt(quantity.getText().toString()));
                intent.putExtra("selectedcaption",selectedString);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

                // Do your another stuff for your onClick

            }
        });
    }

    @Override
    public int getItemCount() {
        return captionListResponseData.size(); // size of the list items

    }

    class CaptionViewHolder extends RecyclerView.ViewHolder {
        // init the item view's
        TextView captionTxt;

        public CaptionViewHolder(View itemView) {
            super(itemView);
            // get the reference of item view's
            captionTxt = (TextView) itemView.findViewById(R.id.txtViewCaption);

        }
    }

}
